import { Component } from '@angular/core';

declare var Module: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wasm';
  multiplyResult: number;
  getLengthResult: number;

  multiply(a: number, b: number): void {
    this.multiplyResult = Module.ccall(
        'multiply', // function name
        'number', // return type
        ['number', 'number'], // argument type
        [a, b] // parameter
    );
  }

  getLength(s: string): void {
    this.getLengthResult = Module.ccall(
        'get_length', // function name
        'number', // return type
        ['string'], // argument type
        [s] // parameter
    );
  }
}
