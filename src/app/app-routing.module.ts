import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ColorcubeComponent} from "./colorcube/colorcube.component";

const routes: Routes = [
  { path: '', component: ColorcubeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
