emcc \
    -O3 \
    -s WASM=1 \
    -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall']" \
    -o webassembly.js \
    main.c
